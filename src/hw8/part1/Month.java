package hw8.part1;

public enum Month {
    JANUARY(1,"ЯНВАРЬ"),
    FEBRUARY(2,"ФЕВРАЛЬ"),
    MARCH(3,"МАРТ"),
    APRIL(4,"АПРЕЛЬ"),
    MAY(5,"МАЙ"),
    JUNE(6,"ИЮНЬ"),
    JULY(7,"ИЮЛЬ"),
    AUGUST(8,"АВГУСТ"),
    SEPTEMBER(9,"СЕНТЯБРЬ"),
    OCTOBER(10,"ОКТЯБРЬ"),
    NOVEMBER(11,"НОЯБРЬ"),
    DECEMBER(12,"ДЕКАБРЬ");

    int numberOfMonth;
    String nameOfMonth;

    Month(int inputNumberOfMonth, String inputNameOfMonth){
        numberOfMonth = inputNumberOfMonth;
        nameOfMonth = inputNameOfMonth;
    }
    public static String getValueOfNumberMonth(int parametrFromScanner){
        Month[] month = Month.values();
        for(int i = 0;i < month.length;i++){
            if(month[i].numberOfMonth == parametrFromScanner){
                return month[i].nameOfMonth;
            }
        }
        return null;
    }
}


