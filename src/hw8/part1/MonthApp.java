package hw8.part1;

import java.util.Scanner;

public class MonthApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        System.out.println(Month.getValueOfNumberMonth(a));
    }
}
