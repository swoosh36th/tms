package hw8.part2;

import java.util.Scanner;

public class Rectangle implements Shape {
    private int width;
    private int height;

    public Rectangle(int inputWidth,int inputHeight){
        this.width = inputWidth;
        this.height = inputHeight;
    }
    @Override
    public void getArea() {
        System.out.println("Площадь прямоугольника = " + height*width );
    }
}
