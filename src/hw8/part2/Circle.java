package hw8.part2;

public class Circle implements Shape{
    private double radius;

    public Circle(double inputRadius){
        this.radius = inputRadius;
    }
    @Override
    public void getArea() {
        double shape2 = 3.14*radius*radius;
        System.out.println("Площадь круга = " + shape2);
    }
}
