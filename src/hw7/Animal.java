package hw7;

import java.util.Date;

public abstract class Animal {
    protected int animalID;
    protected String name;
    protected Date birthday;
    public void setAnimalID(int animalID){
        this.animalID = animalID;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setBirthday(Date birthday){
        this.birthday = birthday;
    }

    public abstract void printInfo();

}
