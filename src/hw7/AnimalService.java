package hw7;

import java.util.Date;

public class AnimalService {
    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date birthday  = new Date();
        cat.setBirthday(birthday);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setAnimalID(2);
        dog.setName("Reks");
        dog.setBirthday(birthday);
        dog.setWeight(50.57);

//  Вывод значений с помощью абстрактного метода напрямую
//        cat.printInfo();
//        dog.printInfo();


//  Вывод значения с помощью метода callPrintAnimal
        callPrintAnimal(cat);
        callPrintAnimal(dog);

        Tiger tiger  = new Tiger();
        tiger.setAnimalID(3);
        tiger.setName("Pushok");
        tiger.setBirthday(birthday);
        tiger.setEyesColor("Black");
        tiger.setCountEatenExployees(8);

        callPrintAnimal(tiger);
    }
    private static void callPrintAnimal(Animal animal){
        animal.printInfo();
    }

}
