package lesson8.example2;

public class SeasonApplication {
    public static void main(String[] args) {
//        System.out.println(Season.SUMMER);
        Season season = Season.WINTER;          //Пример обращения 1 к свойству объекта
        String result = season.getSeason();
        System.out.println(result);
        System.out.println(Season.SPRING.getSeason());     //Пример обращения 2

    }
}
