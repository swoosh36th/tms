package lesson8.example2;

public enum Season {
    WINTER("Зима"),
    SPRING("Весна"),
    SUMMER("Лето"),
    AUTUMN("Осень");


    String seasonValue;
    Season(String seasonValue){
        this.seasonValue = seasonValue;
    }
    public String getSeason(){
        return seasonValue;

    }

}
