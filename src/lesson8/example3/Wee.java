package lesson8.example3;

public enum Wee {

        MONDAY(1, "ПОНЕДЕЛЬНИК"),//ОБЪЕКТЫ
        TUESDAY(2,"ВТОРНИК"),
        WEDNESDAY(3,"СРЕДА"),
        THURSDAY(4,"ЧЕТВЕРГ"),
        FRIDAY(5,"ПЯТНИЦА"),
        SATURDAY(6,"СУББОТА"),
        SUNDAY(7,"ВОСКРЕСЕНЬЕ");

        int numberOfDayWeek; //СВОЙСТВА
        String nameOfDayWeek;


        //КОНСТРУКТОР

        Wee(int inputDayNumbers, String inputnameOfDayWeek ){
                numberOfDayWeek = inputDayNumbers;
                nameOfDayWeek = inputnameOfDayWeek;

        }

        //Статичный метод к которому обращаться только через класс, то есть через класс Wee
        public static String getValueOfDayWeekByNumber(int parametrFromScanner){
                Wee[] week = Wee.values(); //
                for(int i =0; i < week.length;i++){
                        if(week[i].numberOfDayWeek == parametrFromScanner){
                           return week[i].nameOfDayWeek;
                        }
                }
                return null;

        }



}
