package lesson9.example1;

public class TestModel {

    public String name;

    public String getConstName(){
        return "Vasi";
    }

    public String setName(String name){
        this.name = name;
        return this.name;
    }

    public void displayInfo(String pre){
        System.out.println(pre + " " + name);
    }
}
