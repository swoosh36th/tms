package lesson9.cw;

import java.util.Scanner;

public class TestException {
    public static void main(String[] args) {
        try {
        int[] numbers = new int[5];
        Scanner scanner = new Scanner(System.in);
//        System.out.println(scanner.nextInt());
//            В цикле for допущена ошибка при сравнении с длинной массива
        for(int i = 0;i <= numbers.length;i++){
            numbers[i] = scanner.nextInt();
            System.out.println("Получено значение " + numbers[i]);
        }
        }catch(ArrayIndexOutOfBoundsException o){
            System.out.println("Проблема с индексом - " +  o.fillInStackTrace());
        }
        catch(Throwable t){
            System.out.println("Вы ввели что то не то - " + t.fillInStackTrace());
        }finally {
            System.out.println("Рассчет окончен!");
        }
    }
}
