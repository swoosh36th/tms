package lesson9.cw;

import lesson9.cw.User;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserApp {
    public static void main(String[] args) {
        User objUser = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите имя: ");
        try {
            objUser.setName(scanner.next());
        } catch (InputMismatchException e) {
            System.out.println("Введите корректные значения имени или программа закончится");
            System.out.println("Введите имя снова: ");
            objUser.setName(scanner.next());
        }

    }
}
