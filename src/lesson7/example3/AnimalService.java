package lesson7.example3;

import java.util.Date;

public class AnimalService {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.setAnimalID(1);
        cat.setName("Barsik");
        Date data = new Date();
        cat.setData(data);
        cat.setEyesColor("Green");

        Dog dog = new Dog();
        dog.setWeight(56.23);
        dog.setAnimalID(2);
        dog.setName("Sharik");
        dog.setData(data);

        callPrint(cat);
        callPrint(dog);
    }

    private static void callPrint (Animal animal){
        animal.printInfo();
    }
}