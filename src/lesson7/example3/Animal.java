package lesson7.example3;

import java.util.Date;

public abstract class Animal {
    protected int animalID;
    protected String name;
    protected Date data;

    public void setAnimalID(int animalID) {
        this.animalID = animalID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public abstract void printInfo();

}
