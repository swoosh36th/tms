package lesson7.classwork;

public class MessageService {
    public static void main(String[] args) {
        Telegram telegram = new Telegram();
//        telegram.sendMessage();
//        telegram.getMessage();
        callMessenger(telegram);
        WhatsApp whatsapp = new WhatsApp();
//        whatsapp.sendMessage();
//        whatsapp.getMessage();
        callMessenger(whatsapp);
    }
    private static void callMessenger(Messenger messenger){
        messenger.sendMessage();
        messenger.getMessage();
    }
}
