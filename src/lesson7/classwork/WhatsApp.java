package lesson7.classwork;

public class WhatsApp implements Messenger{
    @Override
    public void sendMessage(){
        System.out.println("Отправил сообщение в WhatsApp");
    }
    @Override
    public void getMessage(){
        System.out.println("Получил сообщение в WhatsApp");
    }
}
