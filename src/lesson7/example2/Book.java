package lesson7.example2;

public class Book implements Printable {
    private String name;
    private String author;
    private int year;

    public Book(String inputName, String inputAuthor, int inputYear){
        this.name = inputName;
        this.author = inputAuthor;
        this.year = inputYear;
    }

    @Override
    public void print() {
        System.out.println("Name of book: " + name + " Author: " + author + " Year: " + year);
    }
}
