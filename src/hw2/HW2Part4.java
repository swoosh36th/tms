package hw2;
//В переменную записываете количество программистов.
//В зависимости от количества программистов необходимо вывести правильно окончание.
//Например: • 2 программиста • 1 программист • 10 программистов • и т.д

import java.util.Scanner;

public class HW2Part4 {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in);
        System.out.println("Введите количество программистов:");
        int prog = scanner.nextInt();
             if(prog % 10 == 2 && (prog - 12) % 100 !=0 ||
                prog % 10 == 3 && (prog - 13) % 100 !=0 ||
                prog % 10 == 4 && (prog - 14) % 100 !=0){
            System.out.println(prog + " программиста");
        }else if(prog % 10 == 1 && (prog - 11) % 100 != 0){
            System.out.println(prog + " программист");
        }else if(prog < 0){
                 System.out.println("Введите корректное число программистов!");
             }else {System.out.println(prog + " программистов");
             }
    }
}

