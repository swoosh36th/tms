package hw2;
//Даны 3 целых числа. Найти количество положительных чисел в исходном наборе

public class HW2Part1 {
    public static void main(String[] args) {
        int a = 0;
        int[] nums  = new int[] {-5, 3, 6};
        for(int i = 0;i < 3;i++){
            if(nums[i] > 0){
                a = a + 1;
            }
        }
        System.out.println("Количество положительных чисел - \"" + a + "\"");
    }
}
