package hw2;
//Найти минимум в массиве

public class HW2Part3 {
    public static void main(String[] args) {
        System.out.println("Одномерный массив:");
        int[] arr = new int[]{12,43,-96,57,-9,69};
        for(int i = 0;i < arr.length; i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    minimum(arr);
    }
    private static void minimum(int[] arr){
        int minValue = arr[0];
        for(int i = 1;i < arr.length; i++){
            if(arr[i] < minValue){
                minValue = arr[i];
            }
            }
        System.out.println("Минимальное значение в массиве - \"" + minValue + "\"");
        }
}

