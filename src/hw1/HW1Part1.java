package hw1;

public class HW1Part1 {
    public static void main(String[] args)
    {
    //Даны два числа. Вывести меньшее из них
        int chislo1 = 8;
        float chislo2 = 2.45f;
        if (chislo1 > chislo2) {
            System.out.println(chislo2);
        } else {
            System.out.println(chislo1);
        }

    //Даны два числа. Вывести их сумму
        int ch1 = 11;
        byte ch2 = 55;
        System.out.println(ch1 + ch2);

    //Даны три числа целочисленных типов и с плавающей точкой
    //Вывести их произведение
        int calc1 = 5;
        byte calc2 = 8;
        float calc3 = 5.5f;
        System.out.println(calc1 * calc2 * calc3);

    //Даны два числа. Вывести остаток от деления этих чисел
        byte chis1 = 75;
        byte chis2 = 6;
        System.out.println(chis1 % chis2);

    //Даны 2 числа с плавающей точкой
    //Преобразовать их сумму к целочисленному значение

        double a = 5.88;
        double b = 1.41;
        int c = (int) Math.round(a) + (int) Math.round(b);
        System.out.println(c);
    }
}
