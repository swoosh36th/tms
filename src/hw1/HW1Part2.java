package hw1;

public class HW1Part2 {
    public static void main(String[] args)
    {
        NUMBER(8, 2.45f);
        SUM(11, 55);
        MULTIPLICATION();
        REMNANT(75, 6);
        WHOLE();
    }
    private static void NUMBER(int chislo1, float chislo2)
    {
        //Даны два числа. Вывести меньшее из них
        if (chislo1 > chislo2) {
            System.out.println(chislo2);
        } else {
            System.out.println(chislo1);
        }
    }

    private static void SUM(int ch1, int ch2)
    {
        //Даны два числа. Вывести их сумму
        System.out.println(ch1 + ch2);
    }

    private static void MULTIPLICATION()
    {
        //Даны три числа целочисленных типов и с плавающей точкой
        //Вывести их произведение
        int calc1 = 5;
        byte calc2 = 8;
        float calc3 = 5.5f;
        System.out.println(calc1 * calc2 * calc3);
    }

    private static void REMNANT(int chis1, int chis2)
    {
        //Даны два числа. Вывести остаток от деления этих чисел
        System.out.println(chis1 % chis2);
    }

    private static void WHOLE()
    {
        //Даны 2 числа с плавающей точкой
        //Преобразовать их сумму к целочисленному значение

        double a = 5.88;
        double b = 1.41;
        int c = (int) Math.round(a) + (int) Math.round(b);
        System.out.println(c);
    }
}
