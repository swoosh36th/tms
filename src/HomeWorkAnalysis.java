import java.util.Scanner;

public class HomeWorkAnalysis {
    public static void main(String[] args) {
        sumElement();
//        sortStupid();
//        additionalHomeTask();
    }


    //
   private static void sumElement(){
        int[][] arr = new int[][]{
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        int sumMainDiagonal = 0;
        int sumSideDiagonal = 0;
        for (int i = 0; i < arr.length; i++) {
           for (int j = 0; j < arr[i].length; j++) {
               if (i == j) { // Для главной диагонали
                   sumMainDiagonal = sumMainDiagonal + arr[i][j];
               }

               if((i + j) == (arr.length - 1)) { // Для побочной диагонали
                   sumSideDiagonal = sumMainDiagonal + arr[i][j];
               }
           }
       }
       System.out.println("Сумма элементов на главной диагонали " + sumMainDiagonal);
       System.out.println("Сумма элементов на побочной побочной " +sumSideDiagonal);
   }

    /**
     * 2) Сделать «глупую сортировку» одномерного массива
     * Пример по ссылке https://habr.com/ru/post/204600/
     *  Просматриваем массив слева-направо и по пути сравниваем соседей.
     *  Если мы встретим пару взаимно неотсортированных элементов, то меняем их местами и возвращаемся на круги своя, то бишь в самое начало.
     *  Снова проходим-проверяем массив, если встретили снова «неправильную» пару соседних элементов, то меняем местами и опять начинаем всё сызнова.
     *  Продолжаем до тех пор пока массив потихоньку-полегоньку не отсортируется.
     */
    private static void sortStupid(){
        int[] arr = new int[]{5, 2, 1, 3, 9, 0};


        for (int j = 0; j < arr.length - 1; j++) {
            System.out.println(arr[j] + " " );
                    //+ j + ",");
            //Сравниваем два элемента
            if (arr[j] > arr[j + 1]) {
                //Перестановка
                int tmp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = tmp;
                j = -1;
                System.out.println("Сначала");
            }
        }


        for(int a : arr) {
            System.out.print(a + " ");
        }
    }


    /**
     * 4) * (Дополнительно) В переменную записываете количество программистов.
     * В зависимости от количества программистов необходимо вывести правильно окончание.
     * Например: • 2 программиста • 1 программист • 10 программистов • и т.д
     */
    public static void additionalHomeTask() {
        Scanner scanner  = new Scanner(System.in);
        System.out.println("Введите количество программистов:");
        int prog = scanner.nextInt();
        if(prog % 10 == 2 && (prog - 12) % 100 !=0 ||
                prog % 10 == 3 && (prog - 13) % 100 !=0 ||
                prog % 10 == 4 && (prog - 14) % 100 !=0){
            System.out.println(prog + " программиста");
        } else if(prog % 10 == 1 && (prog - 11) % 100 != 0){
            System.out.println(prog + " программист");
        } else if(prog < 0){
            System.out.println("Введите корректное число программистов!");
        } else {
            System.out.println(prog + " программистов");
        }
    }
}
