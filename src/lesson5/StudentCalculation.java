    package lesson5;



    public class StudentCalculation {
        public static void main(String[] args) {
            Student[] students = new Student[3];
            for (int i = 0; i < students.length; i++) {
                Student student = new Student();
                switch (i) {
                    case 0:
                        student.setName("Masha");
                        student.setGroup(1);
                        student.setGrade(i+3);
                        break;
                    case 1:
                        student.setName("Ivan");
                        student.setGroup(1);
                        student.setGrade(i+3);
                        break;
                    case 2:
                        student.setName("Vasya");
                        student.setGroup(1);
                        student.setGrade(i+3);
                        break;
                    default:
                        student.setName("Dasha");
                        student.setGroup(1);
                        student.setGrade(i+3);

                }
                students[i] = student;
            }
            for(int i = 0; i < students.length;i++){
                System.out.println(students[i].toString());
            }
            }

    }
