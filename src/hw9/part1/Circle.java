package hw9.part1;

import java.util.InputMismatchException;

public class Circle implements Shapes{
    double radius;
    public double getCircle(double radius){
        if(radius <= 0){
            System.out.println("Не корректный радиус круга!");
            throw new InputMismatchException();
        }
        this.radius = radius;
        return this.radius;
    }


    @Override
    public void getPerimeter() {
        double petimeter = 2 * 3.14 * radius;
        System.out.println("Периметр круга = " + petimeter);
    }
}
