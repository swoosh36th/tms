package hw9.part1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class PerimeterApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Circle circle = new Circle();
        System.out.println("Введите радиус круга:");
        try{
            circle.getCircle(scanner.nextDouble());
            circle.getPerimeter();
        }catch(InputMismatchException e){
            System.out.println("Введите корректный радиус круга либо программа остановится!");
            circle.getCircle(scanner.nextDouble());
            circle.getPerimeter();
        }finally {
            System.out.println("Рассчет периметра круга окончен");
        }
        Rectangle rectangle = new Rectangle();
        System.out.println("Введите ширину и длинну прямоугольника:");
        try {
            rectangle.Rectangle(scanner.nextDouble(), scanner.nextDouble());
            rectangle.getPerimeter();
        }catch (InputMismatchException x){
            System.out.println("Введите корректный параметры прямоугольника либо программа остановится!");
            rectangle.Rectangle(scanner.nextDouble(), scanner.nextDouble());
            rectangle.getPerimeter();

        }finally{
            System.out.println("Рассчет периметра прямоугольника закончен");
        }
    }
}
