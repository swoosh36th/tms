package hw9.part1;

import java.util.InputMismatchException;

public class Rectangle implements Shapes{
    double width;
    double height;

    public void Rectangle(double width, double height){
        if(width <= 0 || height <= 0){
            System.out.println("Не корректные параметры!");
            throw new InputMismatchException();
        }
        this.width = width;
        this.height = height;
    }

    @Override
    public void getPerimeter() {
        double petimeter = 2 * (width + height);
        System.out.println("Периметр прямоугольника = " + petimeter);
    }
}
