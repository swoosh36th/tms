package hw9.part2;

import java.util.InputMismatchException;


//делать у Пользвователя свойство Фамилия, и сделать валидацию, чтобы там не было цифр


public class User {
    String surName;

    public String getSurName(String surName) {
        if (surName.contains("1") ||
                surName.contains("2") ||
                surName.contains("3") ||
                surName.contains("4") ||
                surName.contains("5") ||
                surName.contains("6") ||
                surName.contains("7") ||
                surName.contains("8") ||
                surName.contains("9") ||
                surName.contains("0")) {
            System.out.println("Фамилия введена не корректно! ");
            throw new InputMismatchException();
        }
        this.surName = surName;
        return this.surName;
    }
}