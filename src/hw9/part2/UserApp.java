package hw9.part2;

import hw9.part2.User;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        User userSurName = new User();
        System.out.println("Введите фамилию пользователя: ");
        try {
            System.out.println("Фамилия пользователя - " + userSurName.getSurName(scanner.next()));
        }catch (InputMismatchException i){
            System.out.println("Введите валидные данные!");
            System.out.println("Фамилия пользователя - " + userSurName.getSurName(scanner.next()));
        }finally {
            System.out.println("Отработало до конца");
        }
    }
}
