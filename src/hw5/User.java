package hw5;

public class User {
    String login;
    String password;

    public void setLogin(String login){
        this.login = login;
    }

    public void setPassword(String password){
        this.password = password;
    }

    public String toString(){
        return "Логин - " + login + " Пароль - " + password;
    }

}