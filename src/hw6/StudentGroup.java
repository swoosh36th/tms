package hw6;

import java.util.Random;

//Создайте метод класса студент, который будет выводить всю информацию о студенте.
//Выведите информацию о всех студентах из 3 группы в консоль.

public class StudentGroup {
    public static void main(String[] args) {
        String[] name = new String[]{"Vasya", "Petya", "Lera", "Olya", "Kira"};
        Student[] students = new Student[5];
        System.out.println("Все студенты:");
        for(int i = 0;i < students.length;i++){
            Student student = new Student();
            student.setName(name[getRandom(5)]);
            student.setGroup(getRandom(4));
            student.setGrade(getRandom(10));
            students[i] = student;
            System.out.println(students[i].toString());
        }
        System.out.println("Студенты 3 группы:");
        for(int i = 0;i < students.length;i++){
            students[i].only3group();
        }
    }
    private static int getRandom(int Max){
        Random random = new Random();
        return random.nextInt(Max);
    }
}

