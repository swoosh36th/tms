package hw6;

public class Student {
    String name;
    public int group;
    int grade;

    public void setName(String name) {
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public String toString() {
        return "Имя - " + name + " Группа - " + group + " Оценка - " + grade;
    }

    public void only3group(){
        if(group == 3){
            System.out.println(toString());
        }
    }
}
