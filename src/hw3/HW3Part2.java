package hw3;

//Сделать «глупую сортировку» одномерного массива

import java.util.Random;

public class HW3Part2 {
    public static void main(String[] args) {
        stupid();
    }
    private static void stupid(){
        System.out.println("Случайный массив из 10 элементов:");
        int[] arr = new int[10];
        for(int i = 0;i < arr.length;i++){
            arr[i] = randomInt();
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("Глупая сортировка массива по возрастанию:");
        for(int i = 0;i < arr.length - 1;i++){
            if(arr[i] > arr[i + 1]){
                int temp = arr[i + 1];
                arr[i + 1] = arr[i];
                arr[i] = temp;
                i = -1;
            }
        }
        for(int a: arr){
            System.out.print(a + " ");
        }
    }
    private static int randomInt(){
        Random random = new Random();
        return random.nextInt(100);
    }
}
