package hw3;

//Метод sortMax, в котором сортировать элементы одномерного массива по возрастанию
//Метод sortMin, в котором сортировать элементы одномерного массива по убыванию

import java.util.Random;

public class HW3Part1 {
    public static void main(String[] args) {
//        sortMax();
//        System.out.println();
//        sortMin();
    }
    private static void sortMax() {
        System.out.println("Случайный массив из 10 элеменнтов:");
        int[] arr = new int[10];
        for(int i = 0; i < arr.length; i++) {
            arr[i] = randomInt();
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        System.out.println("Отсортированный массив по возрастанию:");
            for(int i = 0;i < arr.length;i++){
                for(int j = 0;j < arr.length - 1;j++){
                    if(arr[j] > arr[j + 1]){
                        int temp = arr[j + 1];
                        arr[j + 1] = arr[j];
                        arr[j] = temp;
                    }
                }
            }
        for(int a : arr){
            System.out.print(a + " ");
    }

    }
    private static void sortMin(){
        System.out.println("Случайный массив из 10 элементов:");
        int[] arr = new int[10];
        for(int i = 0;i < arr.length;i++){
            arr[i] = randomInt();
            System.out.print(arr[i] + " ");
        }
        for(int i =0;i < arr.length;i++){
            for(int j = 0;j < arr.length - 1;j++){
                if(arr[j] < arr[j + 1]){
                    int temp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        System.out.println();
        System.out.println("Отсортированный массив по убыванию:");
        for(int b: arr){
            System.out.print(b + " ");
        }

    }

    private static int randomInt(){
        Random random = new Random();
        return random.nextInt(100);
    }
}
